import { Component } from '@angular/core';

@Component({
    selector: 'footerbar',
    templateUrl: 'footer.component.html',
    styleUrls: ['footer.component.css']
})

export class FooterComponent{}