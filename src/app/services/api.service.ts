import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';
@Injectable()

export class ApiService{
    listUrl = '/api/postsList';


    constructor(private http: HttpClient){}

    getList():Observable<any>{
        return this.http.get<any>(this.listUrl)
        .do(response => console.log(response));
    }
}