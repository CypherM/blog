import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'presentation',
    templateUrl: 'presentation.component.html',
    styleUrls: ['presentation.component.css']
})

export class PresentationComponent implements OnInit{
    user: any;

    constructor(private api: ApiService){}

    ngOnInit(){
        this.api.getList().subscribe( data => {
            this.user = data;
        });
    }

}