import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy{
    list: any;
    subscription: Subscription;
    
    constructor(private api: ApiService){}

    ngOnInit(){
        this.subscription = this.api.getList().subscribe(data => {
         this.list = data;
     })  
    }

    ngOnDestroy(){
       if(this.subscription){
           this.subscription.unsubscribe();
       }
    }


}