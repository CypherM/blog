import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NavComponent } from './components/navigation/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouteModule } from './routing';
import { ApiService } from './services/api.service';
import { PresentationComponent } from './pages/presentation/presentation.component'
import { ApplicationComponent } from './pages/application/aplication.component';
import { PostboardComponent } from './components/postingboard/postingboard.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    FooterComponent,
    PresentationComponent,
    ApplicationComponent,
    PostboardComponent
  ],
  imports: [
    BrowserModule,
    RouteModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
