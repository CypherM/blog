import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ApplicationComponent } from '../app/pages/application/aplication.component';
import { PresentationComponent } from './pages/presentation/presentation.component';

const route: Routes = [
    { path: 'me', component: PresentationComponent},
    { path: 'app', component: ApplicationComponent,
        children:[
            { path: 'home', component: HomeComponent },
        ]},
    { path: '', redirectTo: '/me', pathMatch: 'full'},
    { path: '**', redirectTo: '/me', pathMatch: 'full'}
]

@NgModule({
    imports: [RouterModule.forRoot(route)],
    exports:  [RouterModule]
})

export class RouteModule{}